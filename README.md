# Nomad for armv6l

This repository contains an automation job that compiles Hashicorp's Nomad for armv6l (e.g. Raspberry Pi 0).

Nomad references:

- Repository: https://github.com/hashicorp/nomad

- Website: https://www.nomadproject.io/

- Issue related to Raspberry Pi architecture: https://github.com/hashicorp/nomad/issues/2517

Other references are listed as comments in the [job file](.gitlab-ci.yml).

## Requirements

I've had success building the artifacts in a Vagrant VM (using the libvirt provider) with Arch Linux. You can copy-paste the commands from the job file directly into the VM's shell and it should work without problems.

## Should I use it?

Of course not! Check out the [requirements](https://developer.hashicorp.com/nomad/docs/install/production/requirements) for a Nomad server and draw your conclusions.
